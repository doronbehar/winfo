## WiKi Info - `winfo`

Welcome to 'WiKi Info' - the tool used to create an offline mirror of a [Mediawiki](https://www.mediawiki.org) based website in texinfo format to be read with `info`. The script uses pandoc with pandoc filters to process the 'wikitext' taken from the WiKi website's public API.

Currently, the script was tested with the Arch Linux WiKi.

### Notable features

- Internal links between articles.
- Open external links.

### Installation

### Usage
