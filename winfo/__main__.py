"""Enables running as a module."""

from winfo import main

if __name__ == '__main__':
    SystemExit(main())
