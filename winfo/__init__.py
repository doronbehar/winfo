"""From wiki pages to linked info documentation."""

import shutil
import configparser
import argparse
import os
import sys
import logging

from winfo.wiki import Wiki
from winfo.__version__ import __version__

logging.basicConfig(format='%(message)s')
logger = logging.getLogger(__name__)


def configure_logger(arg_level):
    """Configure the logger according to the log level from argparse."""
    if arg_level > 2:
        logger.propagate = False
        for handler in logger.handlers:
            logger.removeHandler(handler)
        new_handler = logging.StreamHandler()
        new_handler.setFormatter(
            logging.Formatter('%(name)s:%(levelname)s:%(message)s')
        )
        logger.addHandler(new_handler)
        log_level = min(50, arg_level * 10)
        log_level = max(10, 50 - log_level)
        logger.setLevel(log_level)


class ArgpConf(argparse.ArgumentParser):
    """Extends ArgumentParser so for our need."""

    def __init__(self,
                 prog=None,
                 usage=None,
                 description=None,
                 epilog=None,
                 parents=[],
                 formatter_class=argparse.HelpFormatter,
                 prefix_chars='-',
                 fromfile_prefix_chars=None,
                 argument_default=None,
                 conflict_handler='error',
                 add_help=True,
                 allow_abbrev=True):
        """Initialize the possible arguments."""
        super().__init__(prog,
                         usage,
                         description,
                         epilog,
                         parents=parents,
                         formatter_class=formatter_class,
                         prefix_chars=prefix_chars,
                         fromfile_prefix_chars=fromfile_prefix_chars,
                         argument_default=argument_default,
                         conflict_handler=conflict_handler,
                         add_help=add_help,
                         allow_abbrev=allow_abbrev)
        try:
            default_config_location = os.environ['XDG_CONFIG_HOME']
        except KeyError:
            default_config_location = os.environ['HOME'] + "/.config"
        default_config_location += "/winfo/config.ini"
        self.add_argument(
            '-c', '--config',
            help="specify alternative location for configuration file",
            default=default_config_location,
            nargs=1,
        )
        self.add_argument(
            '-p', '--pandoc',
            help="specify alternative location for pandoc executable",
            nargs=1,
        )
        self.add_argument(
            '-v', '--verbose',
            help="increase verbosity",
            default=2,
            action='count',
        )
        self.add_argument(
            '-V', '--version',
            help="show version",
            action='version',
            version='%(prog)s {ver}'.format(ver=__version__)
        )
        self.add_argument(
            'target',
            help="the target wiki you'd like to operate on",
            nargs=1,
        )
        self.args = vars(self.parse_args())
        # handle verbosity
        configure_logger(self.args['verbose'])
        self.args.pop('verbose')
        logger.debug("arguments are: %s", self.args)
        # handle configuration file
        self.config = configparser.ConfigParser()
        logger.info("reading configuration file: %s", self.args['config'])
        self.config.read(self.args['config'])
        self.args.pop('config')
        # handle pandoc executable path from config or arguments
        if 'main' not in self.config:
            self.config['main'] = {}
        if self.args['pandoc']:
            self.config['main']['pandoc'] = self.args['pandoc']
        else:
            if 'pandoc' not in self.config['main']:
                self.config['main']['pandoc'] = shutil.which('pandoc')
        # check if evaluated file is executable
        if not os.path.isfile(self.config['main']['pandoc']):
            logger.error("can't use given pandoc executable %s, since it"
                         " doesn't exist", self.config['main']['pandoc'])
            sys.exit(3)
        if not os.access(self.config['main']['pandoc'], os.X_OK):
            logger.error("can't use given pandoc executable %s, since it"
                         " isn't executable", self.config['main']['pandoc'])
            sys.exit(3)
        logger.info("using executable as pandoc: %s",
                    self.config['main']['pandoc'])
        self.args.pop('pandoc')
        # handle final target configuration
        if self.args['target'][0] not in self.config:
            logger.error("target %s was not found in config file, exiting",
                         self.args['target'][0])
            sys.exit(1)
        self.target_conf = dict(self.config[self.args['target'][0]])
        self.target_conf.update(dict(self.config['main']))


def main():
    """Run."""
    argpconf = ArgpConf(
        description="WiKi pages to info documentation converter"
    )
    return Wiki(argpconf.target_conf).update()
