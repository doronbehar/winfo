"""Wiki class."""

import requests

import pathlib
from datetime import datetime
import subprocess
import time
import json
import logging
import sys
import os

logger = logging.getLogger(__name__)


class Wiki:
    """Wiki class."""

    def __init__(self, conf):
        """Initialize with configuration."""
        self.conf = conf
        self.timestamps_path = self.conf['data'] + "/timestamps.json"
        self.out_dir = self.conf['data']
        self.api = self.conf['api_url']
        logger.info("using base API URL: %s", self.api)
        # check that we can write to the output directory
        if not os.access(self.out_dir, os.W_OK):
            logger.error("can't write to data directory: %s", self.out_dir)
            exit(2)

    def _query(self, params):
        params.update({
            'action': "query",
            'format': "json",
        })
        r = requests.get(self.api, params)
        if r.status_code != 200:
            logger.error("got non 200 status (%d) code for url %s",
                         r.status_code, r.url)
            logger.error("content was: %s", r.content)
            exit(2)
        else:
            logger.debug("retrived data from URL: %s", r.url)
        return r.json()

    def pages(self):
        """Retrive list of all pages."""
        params = {
            'generator': "allpages",
            'prop': "revisions"
        }
        while True:
            res = self._query(params)
            for p_id, p_info in res['query']['pages'].items():
                lastedited = datetime.strptime(
                    p_info['revisions'][0]['timestamp'],
                    "%Y-%m-%dT%H:%M:%SZ"
                )
                lastedited = int(time.mktime(lastedited.timetuple()))
                yield p_id, {
                    'title': p_info['title'],
                    'lastedited': lastedited
                }
            if 'continue' not in res:
                break
            else:
                params.update({
                    'gapfrom': res['continue']['gapcontinue']
                })

    def _get_text(self, p_id):
        # Retrieve wikitext unexpanded
        params = {
            'action': "parse",
            'pageid': p_id,
            'prop': "text",
            'format': "json",
            'redirects': "true"
        }
        r = requests.get(self.api, params)
        if r.status_code != 200:
            logger.error("got non 200 status (%d) code for url %s",
                         r.status_code, r.url)
            logger.error("content in response was: %s", r.content)
            return
        else:
            logger.debug("successfully retrived data from URL: %s", r.url)
        try:
            json_response = r.json()
        except json.JSONDecodeError:
            logger.error("couldn't decode response for page %s as json, "
                         "skipping..", p_id)
            return
        try:
            return json_response['parse']['text']['*'].encode()
        except KeyError:
            if 'error' in json_response:
                logger.warn("error found in response to API call for text of "
                            "page %s: %s", p_id, json_response['error'])
                return
            else:
                logger.warn("couldn't find HTML text in response for API call "
                            "for page: %s, skipping..", p_id)
                logger.debug("response was: %s", json_response)
                return

    def _update_info(self, p_id, title):
        logger.info("updating page %s", title)
        info_path = pathlib.Path("{directory}/{title}.info".format(
            directory=self.out_dir,
            title=title
        ))
        logger.debug("info converted page %s will be saved in %s",
                     title, info_path)
        info_path.parent.mkdir(parents=True, exist_ok=True)
        html = self._get_text(p_id)
        pandoc = subprocess.Popen(
            [
                self.conf['pandoc'], '--from=html', '--to=texinfo',
                # other options
                '--standalone'
            ],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        texinfo, err = pandoc.communicate(input=html)
        if not self._handle_convert_errors(err, title, "pandoc", html,
                                           pandoc.returncode, info_path):
            return False
        makeinfo = subprocess.Popen(
            [
                'makeinfo', '--force',
                '--output={output}'.format(output=info_path)
            ],
            stdin=subprocess.PIPE,
            stdout=sys.stdout,  # no normal output should be printed
            stderr=subprocess.PIPE
        )
        _, err = makeinfo.communicate(input=texinfo)
        if not self._handle_convert_errors(err, title, "makeinfo", html,
                                           makeinfo.returncode, info_path):
            return False
        return True

    def _handle_convert_errors(self, err, title, converter, html, returncode,
                               info_path):
        ret = True
        if returncode != 0:
            logger.warn("%s exited with non zero value (%s), removing and "
                        "skipping page: %s", converter, returncode, title)
            try:
                os.remove(info_path)
            except FileNotFoundError:
                pass
            ret = False
        if err:
            ret = False
            html_save_path = "{directory}/{title}.html".format(
                directory=self.out_dir,
                title=title
            )
            errors_path = "{directory}/{title}.{converter}_errs".format(
                converter=converter,
                directory=self.out_dir,
                title=title
            )
            logger.warn("%s produced errors when converting page %s, they were"
                        " saved in %s", converter, title, errors_path)
            with open(errors_path, "w") as errors_file:
                try:
                    errors_file.write(err.decode("UTF-8"))
                except UnicodeDecodeError:
                    logger.warn("when saving stderr of %s, undecodeable text "
                                "was found so it will be saved as normal str")
                    errors_file.write(str(err))
            logger.warn("original HTML of page %s was saved in %s", title,
                        html_save_path)
            with open(html_save_path, "w") as html_save_file:
                html_save_file.write(html.decode("UTF-8"))
        return ret

    def update(self):
        """Update all data."""
        ret = 0
        try:
            with open(self.timestamps_path) as timestamps_file:
                logger.info("openning timestamps file %s",
                            self.timestamps_path)
                timestamps = json.load(timestamps_file)
        except (json.JSONDecodeError, FileNotFoundError):
            logger.info("encountered decoding error when reading "
                        "timestamps file %s", self.timestamps_path)
            timestamps = {}
        try:
            for p_id, p_info in self.pages():
                try:
                    if timestamps[p_id]['lastedited'] >= p_info['lastedited']:
                        continue
                except KeyError:  # the p_id key may not exist timestamps
                    pass
                timestamps[p_id] = p_info
                # if updating the info file has failed, put a 0 value
                # in the timestamps file for that page
                if not self._update_info(p_id, p_info['title']):
                    timestamps[p_id]['lastedited'] = 0
        except (KeyboardInterrupt, ConnectionError) as err:
            if p_id in timestamps:
                timestamps[p_id]['lastedited'] = 0
            print(err.__class__.__name__, file=sys.stderr)
            ret = 1
        with open(self.timestamps_path, 'w') as timestamps_file:
            json.dump(timestamps, timestamps_file)
        return ret
